# InCA WhichOneIs

InCA WhichOneIs, is a project for ACI research
based on Jennifer Cunha method in Masked Setup Testing, focused on Associative
Tests for small animals. This project purpose is for helping ACI
researchers and also to get Universidad de Chile Computing Engineering
Undergraduate Degree.

## Dependencies

InCA WhichOneIs is a Svelte app, which only needs [Node.js](https://nodejs.org) installed

## Local development

1. Install dependencies to use Svelte
2. Install node dependencies for this project: `npm install`
3. Run in localhost: `npm run dev`
4. Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

If you're using [Visual Studio Code](https://code.visualstudio.com/) we recommend installing the official extension [Svelte for VS Code](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode). If you are using other editors you may need to install a plugin in order to get syntax highlighting and intellisense.

## Deploy static files using SCP on InCA server
1. create a `local` directory
2. locate id_rsa private key to access inca by ssh in `local` directory
3. install dependencies with npm: `npm install`
4. build project: `npm run build`
5. use scp to copy static files on server:
`scp -r -i ./local/id_rsa ./public/*  inca-bat@buho.dcc.uchile.cl:public_html`
